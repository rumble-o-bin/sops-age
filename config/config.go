package config

import (
	"github.com/spf13/viper"
)

type Configurations struct {
	Server         ServerConfigurations
	Database       DatabaseConfigurations
	COOL_VARIABLES string
}

type ServerConfigurations struct {
	Port string
	Mode string
}

type DatabaseConfigurations struct {
	DBname string
	DBuser string
	DBpass string
}

func LoadConfig(path string) (config Configurations, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}
