package router

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Router() *gin.Engine {
	router := gin.Default()
	router.GET("/", helloworld)
	router.GET("/healthz", healthcheck)

	return router
}

func helloworld(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"title": "Super Api",
	})
}

func healthcheck(c *gin.Context) {
	c.String(http.StatusOK, "Im alive!")
}
