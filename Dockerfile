### STAGE 1 : Build the go source code into binary
FROM golang:1.16-alpine as builder
ENV APP_DIR /go/src/sops-age

### Prerequisite
RUN apk --no-cache add build-base git mercurial gcc

# Compile the binary and statically link
RUN mkdir -p ${APP_DIR}
COPY . ${APP_DIR}
RUN cd $APP_DIR; go mod download && CGO_ENABLED=0 go build ${APP_DIR}/cmd/api/main.go

### STAGE 2 : End
FROM alpine:3.13
ENV APP_DIR /go/src/sops-age

RUN mkdir -p /cmd/api
WORKDIR /cmd/api
COPY --from=builder ${APP_DIR}/main .
CMD ["./main"]
