package main

import (
	"copycat/config"
	"copycat/router"
	"log"

	"github.com/gin-gonic/gin"
)

func main() {
	// load config files.
	config, err := config.LoadConfig(".")
	if err != nil {
		log.Fatal("Cannot read config file", err)
	}

	// run the server
	gin.SetMode(config.Server.Mode)
	run := router.Router()
	run.Run(config.Server.Port)
}
